package edu.whut.framework.aop.util;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * @program: MySpring
 * @description: 代理类生成工具
 * @author: Wayne
 * @create: 2020-08-07 11:21
 **/
public class CglibUtil {
    public static Object createProxy(Class<?> clazz, MethodInterceptor mi) {
        return Enhancer.create(clazz, mi);
    }
}
