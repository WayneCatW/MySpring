package edu.whut.framework.aop.annotation;

import java.lang.annotation.*;

/**
 * @program: MySpring
 * @description: 切面注解
 * @author: Wayne
 * @create: 2020-08-07 09:59
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {
    Class<? extends Annotation> value();
}
