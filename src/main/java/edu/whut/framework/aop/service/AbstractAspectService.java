package edu.whut.framework.aop.service;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @program: MySpring
 * @description: 抽象切面逻辑类
 * @author: Wayne
 * @create: 2020-08-07 10:07
 **/
public class AbstractAspectService {
    public void before(Object o, Method method, Object[] objects, MethodProxy methodProxy) {

    }
}
