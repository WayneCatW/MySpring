package edu.whut.framework.aop.service;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: MySpring
 * @description: 带顺序的切面执行类
 * @author: Wayne
 * @create: 2020-08-07 10:18
 **/
@AllArgsConstructor
@Data
public class OrderAspectService {
    private AbstractAspectService aspectService;
    private int order;
}
