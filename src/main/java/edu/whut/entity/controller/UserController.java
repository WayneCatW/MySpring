package edu.whut.entity.controller;

import edu.whut.entity.service.IUserService;
import edu.whut.framework.annotation.Autowired;
import edu.whut.framework.annotation.Controller;

/**
 * @program: MySpring
 * @description: 用户控制器
 * @author: Wayne
 * @create: 2020-05-25 21:47
 **/
@Controller
public class UserController {


    private IUserService userService;

    @Autowired(name="WUserServiceImpl")
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public void demo() {
        System.out.print("执行方法");
    }
}
