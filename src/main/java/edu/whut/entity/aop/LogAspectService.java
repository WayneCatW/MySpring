package edu.whut.entity.aop;

import edu.whut.framework.annotation.Component;
import edu.whut.framework.annotation.Controller;
import edu.whut.framework.aop.annotation.Aspect;
import edu.whut.framework.aop.annotation.Order;
import edu.whut.framework.aop.service.AbstractAspectService;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @program: MySpring
 * @description: 日志切面类
 * @author: Wayne
 * @create: 2020-08-07 10:14
 **/
@Aspect(value= Controller.class)
@Order(value=1)
@Component
public class LogAspectService extends AbstractAspectService {
    @Override
    public void before(Object o, Method method, Object[] objects, MethodProxy methodProxy) {
        System.out.println("日志记录开始");

    }
}
