package edu.whut.framework.test;

import com.google.common.collect.Lists;
import edu.whut.entity.controller.UserController;
import edu.whut.entity.pojo.Student;
import edu.whut.framework.aop.util.Weaver;
import edu.whut.framework.container.BeanContainer;
import edu.whut.framework.inject.DependencyInjector;
import edu.whut.framework.util.ClassUtil;

import java.util.ArrayList;

/**
 * @program: MySpring
 * @description:
 * @author: Wayne
 * @create: 2020-05-24 23:40
 **/
public class Test {

    public static void main(String[] args) throws Exception {
        BeanContainer beanContainer = BeanContainer.getInstance();
        beanContainer.loadBeans("edu.whut.entity");
        new DependencyInjector().doAutowired();
        Weaver weaver = new Weaver();
        weaver.findJointPoint();
        UserController controller = (UserController)beanContainer.getBean(UserController.class);
        controller.demo();
    }

}
